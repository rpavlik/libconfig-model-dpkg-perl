use strict;
use warnings;

my $conf_file_name = "some-patch";

my $conf_dir       = 'debian/patches';
my $model_to_test  = "Dpkg::Patch";

my @tests = (
    {
        name => 'libperl5i' ,
        backend_arg => $conf_file_name,
        log4perl_load_warnings => [[
            'User',
            warn => qr/https protocol should be used instead of http/,
            warn => qr/This field should contain an URL to Debian BTS and not just a bug number/,
            warn => qr/https protocol should be used instead of http/,
            warn => qr/Unknown host or protocol for Debian BTS/,
            warn => qr/This field should contain an URL to Debian BTS and not just a bug number/,
            warn => qr/https protocol should be used instead of http/,
            warn => qr/Unknown host or protocol for Debian BTS/,
        ]],
        load =>'Bug-Debian:.push(" http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=768073")',
        apply_fix => 1 ,
        check => {
            'Synopsis' => qr/test TODO/,
            'Subject' => qr/utf8/,
            'Subject' => qr/879/,
            'Subject' => qr/2012/,
            'Bug:0' => 'https://github.com/schwern/perl5i/issues/218',
            'Bug:1' => 'https://github.com/schwern/perl5i/issues/219',
            'Origin' => 'https://github.com/doherty/perl5i',
            'Bug-Debian:0' => 'https://bugs.debian.org/655329',
            'Bug-Debian:3' => 'https://bugs.debian.org/768073',
        },
        # file_check_sub => $del_home,
    },
    {
        name => 'moarvm' ,
        backend_arg => $conf_file_name,
        check => {
            'Synopsis' => qr/Configure.pl/,
        },
        # check that subject stays first field
        file_contents_like => {
            "$conf_dir/$conf_file_name" => [ qr/^Subject/ ] ,
        }
    },
    {
        name => 'by-git',
        backend_arg => $conf_file_name,
        dump_warnings => [ qr/Empty/ ],
        check => {
            Description => qr/enhance/,
            diff => qr/@@ -7,7/
        },
        # check that description is first field
        file_contents_like => {
            "$conf_dir/$conf_file_name" => [ qr/^Description/ ] ,
        }

    },
    {
        name => 'bare-patch',
        backend_arg => $conf_file_name,
        log4perl_load_warnings => [[
        ]],
        apply_fix => 1,
        check => {
            Synopsis => 'Some patch',
        }
    }
);

return {
    conf_file_name => $conf_file_name,
    conf_dir => $conf_dir,
    tests => \@tests,
};
