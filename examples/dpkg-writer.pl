package MyParser ;

use strict;
use warnings;

use 5.20.1;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($WARN);

# load role
use Mouse ;
with 'Config::Model::Backend::DpkgSyntax';

package main ;
use IO::File;
use Data::Dumper;

my $data = [
    [ '# section comment', qw/Name Foo/, '# data comment', qw/Version 1.2/ ],
    [
        qw/Name Bar Version 1.3/ ,
        Files => [qw/file1/, [ 'file2' , '# inline comment'] ] ,
        Description => "A very\n\nlong description"
    ]
];

my $parser = MyParser->new() ;


my $fhw = IO::File->new ;
$fhw -> open ( 'examples/dpkg-new' ,'>',"," ) ;

$parser->write_dpkg_file($fhw,$data) ;
$fhw->close;
